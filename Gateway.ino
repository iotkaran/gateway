/*
 Name:		Tree_Mesh2.ino
 Created:	3/18/2020 11:44:25 AM
 Author:	Mohammad
*/
#include "Global.h"
#include "Function.h"

void setup()
{
    Serial.begin(115200);
    Serial.println(ESP.getResetReason());
    EEPROM.begin(800);
    // EEPROM
    Registerd = F.EEPROM_readString(0).toInt();
    if (Registerd == 65535)
    {
        GateWay_Token = F.EEPROM_readString(10);
        AP_SSID = F.EEPROM_readString(100);
        AP_PASS = F.EEPROM_readString(200);
        Main_SSID = F.EEPROM_readString(300);
        Main_PASS = F.EEPROM_readString(400);
        AA = F.EEPROM_readString(500);
        Serial.println("Normal Mode");
        Serial.println(AP_SSID);
        Serial.println(AP_PASS);
        Serial.println(Main_SSID);
        Serial.println(Main_PASS);
        Serial.println(GateWay_Token);
        Serial.println(AA);
        F.Allowed_AccessPoint_Manager();
        Connection_Type = 1;
    }
    else
    {
        Serial.println("Config Mode");
        Connection_Type = 5;
    }
    // EEPROM
    chipid = ESP.getChipId();
    //pinMode(LED1_Pin, OUTPUT);
    //pinMode(Relay1_Pin, OUTPUT);                       // LED 1
    pinMode(Wifi_LED_Pin, OUTPUT);
    digitalWrite(Wifi_LED_Pin, LOW);
    //digitalWrite(LED1_Pin, HIGH);
    //digitalWrite(Relay1_Pin, LOW);
    pinMode(Touch1_Pin, INPUT);                      // Touch 1
    WiFi.setOutputPower(82);
    WiFi.setPhyMode(WIFI_PHY_MODE_11B);
    delay(500);
    // Ping Setup
}

void loop()
{
    if (Connection_Type)
    {
        switch (Connection_Type)
        {
        case 1:
        {
            F.Start_AP();
            break;
        }
        case 2:                            // Connect To WiFi
        {
            if ((millis() - Last_Scan_Network >= 10000 && try_wifi != 100) | ST_SSID == "")
            {
                if (F.Manage_WiFi())
                {
                    try_wifi = 100;
                }
                Last_Scan_Network = millis();
            }
            if (ST_SSID != "")
            {
                F.Connect_To_WiFi();
            }
            break;
        }
        case 3:                                        // Connect To Server
        {
            F.Connect_To_Server();
            break;
        }
        case 4:                                        // Start Server
        {
            F.Start_WiFiServer();
            break;
        }
        case 5:
        {
            F.Start_AP_Phone();
            server.on("/", handle_root);
            server.on("/action_page", handleForm);
            server.begin();
            delay(500);
            break;
        }
        case 6:
        {
            F.StayToResponse();
            break;
        }
        }
    }
    if (Active_Mode)
    {
        switch (Active_Mode)
        {
        case 1:
        {
            // If Receive Data From Server
            int i_Num_Check;
            for (int i_Sweep = 0 ; i_Sweep < Max_Clients; i_Sweep++)
            {
                if (Client_Valid[i_Sweep])
                {   
                    i_Num_Check++;
                    if (Client[i_Sweep].available())//client_GateWay.available())
                    {
                        Serial.println("====================================");
                        String Received_Data = Client[i_Sweep].readStringUntil('\n');    // Read Message
                        Serial.print("Receive Server : ");
                        Serial.println(Received_Data);
                        Data_Server_Bridge = Received_Data;
                        char data[1000];
                        Data_Server_Bridge.toCharArray(data, 1000);
                        client_DOWN.print(data);
                        Serial.println("Server To Client");
                    }
                    if (i_Num_Check == Num_Clients)
                    {
                        break;
                    }
                }
            }
            // Listen For New Client
            if (!client_DOWN.connected() | !client_DOWN)                          // If There Isn't Any Client  
            {
                client_DOWN = wifiServer.available();                        // Listen For New Client
            }
            // If Receive Data From Client
            if (client_DOWN.available())                                     // If There Is A Message From Client
            {
                String Received_Data = client_DOWN.readStringUntil('\n');    // Read Message
                Serial.print("Receive Client : ");
                Serial.println(Received_Data);
                F.Manage_Clients(Received_Data);
            }
            if (New_Connection)
            {
                IP = Server_IP;
                PORT = Server_PORT;
                Serial.print("Connecting to : ");
                Serial.println(IP);
                char ip[200];
                IP.toCharArray(ip, 200);
                Client[Client_Pointer].setTimeout(300);
                Client[Client_Pointer].connect(ip, PORT.toInt());
                delay(200);
                if (!Client[Client_Pointer].connected() && !Client[Client_Pointer])
                {
                    Serial.println("Not Connect To Server");						// Reconnect To Server
                }
                else
                {
                    Serial.println("Connected to Server");
                    Serial.println(Client_Pointer);
                    F.Send_To_Server(Data_Client_Bridge + '\n');
                    New_Connection = 0;
                }
            }
            // Check For Connection
            if (millis() - Last_Check_Connection >= 10000)              // Check Every 10 Seconds
            {
                if (WiFi.status() != WL_CONNECTED)                      // Check Wifi Connection
                {
                    Serial.println("Wifi disconnected");
                    Wifi_Connected = 0;
                    Server_Connected = 0;
                    client_DOWN.stop();
                    wifiServer.close();
                    int i_Num_Check;
                    for (int i_Sweep = 0; i_Sweep < Max_Clients; i_Sweep++)
                    {
                        i_Num_Check++;
                        if (Client_Valid[i_Sweep])
                        {
                            Client[i_Sweep].stop();
                            Client_Valid[i_Sweep] = 0;
                        }
                        if (i_Num_Check == Num_Clients)
                        {
                            break;
                        }
                    }
                    Num_Clients = 0;
                    client_GateWay.stop();
                    WiFi.disconnect(true);
                    Active_Mode = 0;
                    Connection_Type = 2;                                // Reconnect Wifi
                }
                else if (!client_GateWay.connected() | !client_GateWay)                 // Check Server Connection
                {
                    Serial.println("Server disconnected");
                    Server_Connected = 0;
                    client_DOWN.stop();
                    wifiServer.close();
                    int i_Num_Check;
                    for (int i_Sweep = 0; i_Sweep < Max_Clients; i_Sweep++)
                    {
                        i_Num_Check++;
                        if (Client_Valid[i_Sweep])
                        {
                            Client[i_Sweep].stop();
                            Client_Valid[i_Sweep] = 0;
                        }
                        if (i_Num_Check == Num_Clients)
                        {
                            break;
                        }
                    }
                    Num_Clients = 0;
                    client_GateWay.stop();
                    Active_Mode = 0;
                    Connection_Type = 3;                                // Reconnect Server
                }
                //if (Send_ping && Wifi_Connected && Server_Connected)
                //{
                //    Serial.println("Send Ping");
                //    pinger.Ping(Server_IP, 3);
                //    Send_ping = 0;
                //}
                Last_Check_Connection = millis();
            }
            break;
        }
        case 2:
        {
            if (!client_Phone.connected() | !client_Phone)                          // If There Isn't Any Client  
            {
                client_Phone = wifiServer_Phone.available();                        // Listen For New Client
            }
            if (client_Phone.available())                                     // If There Is A Message From Client
            {
                String Received_Data = client_Phone.readStringUntil('\r');    // Read Message
                Serial.print("Receive Phone : ");
                Serial.println(Received_Data);
                if (F.Recive_Jason(Received_Data))                   // If Protocol Correct
                {
                    F.Command_Phone_Switch();                        // Do The Command And Response
                }
            }
            server.handleClient();
            break;
        }
        }
    }

    F.Switch_Manager();                            // Check Switch Status
    F.Wifi_LED();
}

void handle_root()
{
    for (int i = 0; i <= 100; i++)
    {
        String j = String(i);
    }
    Serial.println(MAIN_page);
    delay(5000);
    String A  =  R"=====(<!DOCTYPE html>
        <html>
        <body>
        <h2>IOTKARAN< / h2>
        <form action = "/action_page">
        <label for = "ASSID">Access Point SSID : < / label><br>
        <input type = "text" name = "ASSID" value = "" autofocus><br>
        <label for = "APASS">Access Point Pass : < / label><br>
        <input type = "password" name = "APASS" value = ""><br>
        <label for = "MSSID">Main SSID : < / label><br>
        <input type = "text" name = "MSSID" value = ""><br>
        <label for = "MPASS">Main Pass : < / label><br>
        <input type = "password" name = "MPASS" value = ""><br>
        <label for = "AA">Allowed Access Points : < / label><br>
        <input type = "text" name = "AA" value = "" placeholder = "/1/2/3/4/"><br><br>
        <input type = "submit" value = "Submit">
        < / form>
        < / body>
        < / html>)=====";
    delay(5000);
    Serial.println("Web Request2");
    server.send(200, "text/html", A);
}
void handleForm()
{
    String ASSID = server.arg("ASSID");
    String APASS = server.arg("APASS");
    String MSSID = server.arg("MSSID");
    String MPASS = server.arg("MPASS");
    String AA = server.arg("AA");
    F.EEPROM_writeString(100, ASSID);
    F.EEPROM_writeString(200, APASS);
    F.EEPROM_writeString(300, MSSID);
    F.EEPROM_writeString(400, MPASS);
    F.EEPROM_writeString(500, AA);

    Serial.print("AP_SSID:");
    Serial.println(ASSID);

    Serial.print("AP_PASS:");
    Serial.println(APASS);

    Serial.print("Main_SSID:");
    Serial.println(MSSID);

    Serial.print("Main_PASS:");
    Serial.println(MPASS);

    Serial.print("AA:");
    Serial.println(AA);

    String s = "<a href='/'> Go Back </a>";
    server.send(200, "text/html", s);
    delay(3000);
    ESP.restart();
}