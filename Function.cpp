#include "Global.h"
#include "Function.h"

void Function::Start_AP()
{
	char SSID[200];
	char PASS[200];
	AP_SSID.toCharArray(SSID, 200);
	AP_PASS.toCharArray(PASS, 200);
	WiFi.mode(WIFI_AP);
	delay(100);
	Connection_Type = 2;
	int ip_set = AP_SSID.substring(AP_SSID.indexOf("_") + 1).toInt();
	WiFi.softAPConfig(IPAddress(ip_set, 130, 1, 1), IPAddress(ip_set, 130, 1, 1), IPAddress(255, 255, 255, 0));
	delay(100);
	WiFi.softAP(SSID, PASS);
	IPAddress myIP = WiFi.softAPIP();
	Serial.println(myIP);
	delay(200);
}

void Function::Connect_To_WiFi()
{
	if (try_wifi++ >= 100)
	{
		char SSID[200];
		char PASS[200];
		ST_SSID.toCharArray(SSID, 200);
		ST_PASS.toCharArray(PASS, 200);
		WiFi.mode(WIFI_AP_STA);
		delay(10);
		WiFi.begin(SSID, PASS, 11);
		Serial.print("Waiting for WiFi... ");
		Serial.print("Connecting to ");
		Serial.println(ST_SSID);
		Wifi_Connected = 0;
		try_wifi = 0;
	}
	delay(100);
	if (WiFi.status() != WL_CONNECTED)
	{
		Connection_Type = 2;
	}
	else
	{
		Wifi_Connected = 1;
		Serial.println("WiFi connected");
		Serial.println("IP address: ");
		Serial.println(WiFi.localIP());
		Connection_Type = 3;								// Connect To Server 
		try_wifi = 100;
	}
}

void Function::Connect_To_Server()
{
	IP = Server_IP;
	PORT = Server_PORT;
	Serial.print("Connecting to : ");
	Serial.println(IP);
	char ip[200];
	IP.toCharArray(ip, 200);
	client_GateWay.setTimeout(300);
	client_GateWay.connect(ip, PORT.toInt());
	delay(200);
	if (!client_GateWay.connected() && !client_GateWay)
	{
		Serial.println("Not Connect To Server");
		Connection_Type = 3;							// Reconnect To Server
	}
	else
	{
		delay(100);
		Serial.println("Connected to Server");
		String Log_In = Send_Jason(GateWay_Type, GateWay_Token, 1, "S", Server_ID, 1, "2019-12-01 12:22:21", "Request", "Get", 5, String(""));
		Send_To_Server(Log_In);
		Server_Connected = 1;
		Connection_Type = 6;
	}
}

bool Function::Manage_WiFi()
{
	bool State = 0;
	String Selected_SSID = "";
	int n = WiFi.scanNetworks();
	if (n == 0)
	{
		Serial.println("No Networks Found");
	}
	else
	{
		for (int i = 0; i < n; i++)
		{
			Serial.print(WiFi.SSID(i));
			for (int j = 0; j < Num_Allowed_SSIDs; j++)
			{
				if (Allowed_SSIDs[j] == WiFi.SSID(i) && Max_Power + 10 <= WiFi.RSSI(i))
				{
					Serial.print(WiFi.RSSI(i));
					Selected_SSID = Allowed_SSIDs[j];
					State = 1;
					//if (Allowed_SSIDs[j] != ST_SSID)
					//{
					//	Serial.println("Chose");

					//}
					Max_Power = WiFi.RSSI(i);
				}
			}
			Serial.println("");
		}
	}
	if (State)
	{
		if (Selected_SSID == ST_SSID)
		{
			State = 0;
		}
		else
		{
			ST_SSID = Selected_SSID;
			if (ST_SSID == Main_SSID)
			{
				ST_PASS = Main_PASS;
				Serial.println(ST_PASS);
			}
			else
			{
				ST_PASS = AP_PASS;
				Serial.println(ST_PASS);
			}
		}
	}
	Max_Power = -100;
	return State;
}

void Function::Start_WiFiServer()
{
	Serial.println("Start Server");
	wifiServer = WiFiServer(IPAddress(100, 130, 1, 1), 2323);
	delay(50);
	wifiServer.begin();
	delay(50);
	Connection_Type = 0;
	Active_Mode = 1;
}

void Function::Switch_Manager()
{
	//if (Switch_Status_Change)
	//{
	//	switch (Switch_Status_Array[0].toInt())				// Switch For Touch 1 Status Change
	//	{
	//	case 0:
	//	{
	//		Serial.println("LED 1:OFF");
	//		digitalWrite(Relay1_Pin, LOW);						// Turn Off LED 1
	//		digitalWrite(LED1_Pin, HIGH);
	//		Switch_Status_Change = 0;
	//		break;
	//	}
	//	case 1:
	//	{
	//		Serial.println("LED 1:On");
	//		digitalWrite(Relay1_Pin, HIGH);					// Turn On LED 1
	//		digitalWrite(LED1_Pin, LOW);
	//		Switch_Status_Change = 0;
	//		break;
	//	}
	//	}
	//}

	Touch1 = digitalRead(Touch1_Pin);
	if (Touch1 && Touch1_State == 0)
	{
		Touch1_Timer = millis();
		Touch1_State = 1;
	}
	//else if (!Touch1 && Touch1_State == 1)
	//{
	//	if (millis() - Touch1_Timer >= 20)
	//	{
	//		Serial.println("Touch1 Keep For 20 Milli Seconds");
	//		digitalWrite(Relay1_Pin, !digitalRead(Relay1_Pin));
	//		digitalWrite(LED1_Pin, !digitalRead(Relay1_Pin));
	//		Switch_Status_Array[0] = String(digitalRead(Relay1_Pin));
	//		if (client_UP.connected())
	//		{
	//			Switch_Status = Switch_Status_Array[0];
	//			String status = Send_Jason(Switch_Type, GateWay_Token, 1, "S", Server_ID, 0, "2019-12-01 12:22:21", "Request", "Set", 2, Switch_Status);
	//			Send_To_Server(status);
	//		}
	//	}
	//	Touch1_State = 0;
	//}
	else if (Touch1 && Touch1_State == 1)
	{
		if (millis() - Touch1_Timer >= 3000)
		{
			Serial.println("Touch1 Keep For 3 Seconds");
			Connection_Type = 5;                        // Switch Start As Access Point
			Active_Mode = 0;
			Touch1_State = 2;
		}
	}
	//else if (Touch1 && Touch1_State == 2)
	//{
	//	if (millis() - Touch1_Timer >= 7000)
	//	{
	//		Serial.println("Touch1 Keep For 7 Seconds");
	//		ESP.restart();
	//	}
	//}
	else if (!Touch1 && Touch1_State == 2)
	{
		Touch1_State = 0;
	}
}

void Function::Wifi_LED()
{
	if (Wifi_Connected && Server_Connected && !Config_Mode)
	{
		digitalWrite(Wifi_LED_Pin, HIGH);
	}
	else if (Wifi_Connected && !Server_Connected && !Config_Mode)
	{
		if (millis() - Timer_LED >= 100)
		{
			digitalWrite(Wifi_LED_Pin, !digitalRead(Wifi_LED_Pin));
			Timer_LED = millis();
		}
	}
	else if (!Wifi_Connected && !Server_Connected && Config_Mode)
	{
		if (millis() - Timer_LED >= 1000)
		{
			digitalWrite(Wifi_LED_Pin, !digitalRead(Wifi_LED_Pin));
			Timer_LED = millis();
		}
	}
	else if (!Wifi_Connected && !Server_Connected && !Config_Mode)
	{
		if (millis() - Timer_LED >= 500)
		{
			digitalWrite(Wifi_LED_Pin, !digitalRead(Wifi_LED_Pin));
			delay(50);
			digitalWrite(Wifi_LED_Pin, !digitalRead(Wifi_LED_Pin));
			delay(50);
			digitalWrite(Wifi_LED_Pin, !digitalRead(Wifi_LED_Pin));
			delay(50);
			digitalWrite(Wifi_LED_Pin, !digitalRead(Wifi_LED_Pin));
			delay(50);
			Timer_LED = millis();
		}
	}
}

void Function::EEPROM_writeString(char add, String data)
{
	int _size = data.length();
	int i;
	for (i = 0; i < _size; i++)
	{
		EEPROM.write(add + i, data[i]);
	}
	EEPROM.write(add + _size, '\0');   //Add termination null character for String Data
	EEPROM.commit();
}


String Function::EEPROM_readString(char add)
{
	int i;
	char data[500]; //Max 100 Bytes
	int len = 0;
	unsigned char k;
	k = EEPROM.read(add);
	while (k != '\0' && len < 500)   //Read until null character
	{
		k = EEPROM.read(add + len);
		data[len] = k;
		len++;
	}
	data[len] = '\0';
	return String(data);
}

bool Function::Recive_Jason(String message)
{
	//link example jason 6 object 
	//https://arduinojson.org/v6/api/jsonobject/createnestedobject/
	//define data to object jason1
	StaticJsonDocument<500> Massage;
	deserializeJson(Massage, message);
	String From = Massage["From"];
	String To = Massage["To"];
	String Info = Massage["Info"];
	StaticJsonDocument<500> jason_From;
	char from[1000];
	From.toCharArray(from, 1000);
	char to[1000];
	To.toCharArray(to, 1000);
	char info[1000];
	Info.toCharArray(info, 1000);
	deserializeJson(jason_From, from);
	StaticJsonDocument<500> jason_To;
	deserializeJson(jason_To, to);
	StaticJsonDocument<500> jason_Info;
	deserializeJson(jason_Info, info);
	String Data = jason_Info["Data"];
	char data[1000];
	Data.toCharArray(data, 1000);
	StaticJsonDocument<500> jason_Data;
	deserializeJson(jason_Data, data);

	// Values 
	String type_from = jason_From["Type"];
	String Token_from = jason_From["Token"];
	if (Token_from == "null")
	{
		Serial.println("Protocol_Error1");
		return 0;
	}
	Token_From = Token_from;
	int rule_from = jason_From["Rule"];
	String type_to = jason_To["Type"];
	String Token_to = jason_To["Token"];
	int rule_to = jason_To["Rule"];
	String time = Massage["Time"];
	String type_info = jason_Info["Type"];
	String action = jason_Info["Action"];
	int command = jason_Info["Command"];
	if (type_from == "null" | type_to == "null" | Token_to == "null" | time == "null" | type_info == "null" | action == "null" | command == NULL)   // Check Protocol
	{
		Serial.println("Protocol_Error2");
		return 0;
	}
	Type_From = type_from;
	Rule_From = rule_from;
	Token_To = Token_to;
	Type_To = type_to;
	Rule_To = rule_to;
	Time = time;
	Type_Info = type_info;
	Action = action;
	Command = command;
	Serial.println("--------------------");
	Serial.println(Token_To);
	Given_Token = Token_To;
	Serial.println("--------------------");
	switch (Command)																							// Detect Command Type
	{
	case 1:
	{
		String wifi = jason_Data["wifi"];
		String pass = jason_Data["pass"];
		String user = jason_Data["user_register"];
		int rule = jason_Data["Rule"];
		String Token = jason_Data["Token"];
		Serial.println("--------------------");
		Serial.println(Token);
		Serial.println("--------------------");
		if (wifi == "null" | pass == "null")//| user == "null" )									    // Check Protocol
		{
			Serial.println("Protocol_Error3");
			return 0;
		}
		Given_ssid = wifi;
		Given_pass = pass;
		Given_User = user;
		Given_Rule = rule;

		break;
	}
	case 2:
	{
		String status = jason_Data["Status"];
		if (status == "null")																				    // Check Protocol
		{
			Serial.println("Protocol_Error4");
			return 0;
		}
		Given_Status = status;
		Switch_Status_Array[0] = status;
		break;
	}
	case 3:
	{
		String wifi = jason_Data["wifi"];
		String pass = jason_Data["pass"];
		String user = jason_Data["user_register"];
		int rule = jason_Data["Rule"];
		if (wifi == "null" | pass == "null" | user == "null")										// Check Protocol
		{
			Serial.println("Protocol_Error5");
			return 0;
		}
		Given_ssid = wifi;
		Given_pass = pass;
		Given_User = user;
		Given_Rule = rule;
		break;
	}
	case 4:
	{
		Given_Factory_Reset = jason_Data["factory"];
		if (Given_Factory_Reset == NULL)
		{
			Serial.println("Protocol_Error6");
			return 0;
		}
		break;
	}
	case 5:
	{
		// Nothing
		break;
	}
	case 7:
	{
		String link_ota = jason_Data["link"];
		Link_OTA = link_ota;
		break;
	}
	case 15:
	{
		int Mode = jason_Data["Mode"];
		String wifi = jason_Data["wifi"];
		String pass = jason_Data["pass"];
		Given_Active_Mode = Mode;
		Given_ssid = wifi;
		Given_pass = pass;
		break;
	}
	default:
	{
		Serial.println("Command Not Available");
		return 0;
		break;
	}
	}
	return 1;
}


String Function::Send_Jason(String Type_From, String Token_From, int Rule_From, String Type_To, String Token_To, int Rule_To, String Time, String Type_Info, String Action, int Command, String Status)
{
	//link example jason 6 object 
	//https://arduinojson.org/v6/api/jsonobject/createnestedobject/

	//object jason for send
	StaticJsonDocument<500> doc;
	JsonObject Message = doc.to<JsonObject>();
	JsonObject jason_From = Message.createNestedObject("From");
	JsonObject jason_To = Message.createNestedObject("To");
	JsonObject jason_Info = Message.createNestedObject("Info");
	JsonObject jason_Data = jason_Info.createNestedObject("Data");

	jason_From["Type"] = Type_From;
	jason_From["Token"] = Token_From;
	jason_From["Rule"] = Rule_From;
	jason_To["Type"] = Type_To;
	jason_To["Token"] = Token_To;
	jason_To["Rule"] = Rule_To;
	Message["Time"] = Time;
	jason_Info["Type"] = Type_Info;
	jason_Info["Action"] = Action;
	jason_Info["Command"] = Command;

	switch (Command)
	{
	case 1:
	{
		jason_Data["wifi"] = Main_SSID;
		jason_Data["pass"] = Main_PASS;
		jason_Data["mac"] = getMacAddress();                                              // Get Mac Address
		jason_Data["user_register"] = User;
		jason_Data["Rule"] = Rule;
		jason_Data["Token"] = GateWay_Token;
		break;
	}
	case 2:
	{
		jason_Data["Status"] = Status;
		break;
	}
	case 3:
	{
		jason_Data["wifi"] = Main_PASS;
		jason_Data["pass"] = Main_PASS;
		jason_Data["mac"] = getMacAddress();                                             // Get Mac Address
		jason_Data["user_register"] = User;
		jason_Data["Rule"] = Rule;
		break;
	}
	case 4:
	{
		jason_Data["factory"] = Factory_Reset;
		break;
	}
	case 5:
	{
		// Nothing
		jason_Data["Test"] = WiFi.RSSI();
		break;
	}
	case 7:
	{
		jason_Data["link"] = Link_OTA;
		break;
	}
	case 15:
	{
		jason_Data["Mode"] = Given_Active_Mode;
		switch (Given_Active_Mode)
		{
		case 1:
		{
			//jason_Data["wifi"] = Access_Point_ssid;
			//jason_Data["pass"] = Access_Point_Pass;
			break;
		}
		case 2:
		{
			jason_Data["wifi"] = Main_SSID;
			jason_Data["pass"] = Main_PASS;
			break;
		}
		}
		break;
	}

	}
	String finalJson = "";
	serializeJson(Message, finalJson);
	return (finalJson + "\n");
}

void Function::Send_To_Server(String Message)
{
	Serial.print("Send To Server : ");
	Serial.println(Message);
	char res[1000];
	Message.toCharArray(res, 1000);
	client_GateWay.print(res);											// Send Message To Server
}

String Function::getMacAddress()
{
	WiFi.macAddress();
	return String(WiFi.macAddress());
}

void Function::Start_AP_Phone()
{
	Serial.println("Start Access Point");
	if (client_DOWN.connected() | client_DOWN)
	{
		client_DOWN.stop();
		Serial.println("client_DOWN Disconnect");
	}
	if (client_GateWay.connected() | client_GateWay)
	{
		client_GateWay.stop();
		Serial.println("client_GateWay Disconnect");
	}
	if (Num_Clients)
	{
		Serial.print("Num_Clients : ");
		Serial.println(Num_Clients);
		int i_Num_Check;
		for (int i_Sweep = 0; i_Sweep < Max_Clients; i_Sweep++)
		{
			Serial.println("Disconnecting");
			i_Num_Check++;
			if (Client_Valid[i_Sweep])
			{
				Client[i_Sweep].stop();
				Client_Valid[i_Sweep] = 0;
			}
			if (i_Num_Check == Num_Clients)
			{
				break;
			}
		}
		Num_Clients = 0;
	}
	if (WiFi.status() == WL_CONNECTED)
	{
		WiFi.disconnect(true);
	}
	String ap_ssid = AP_Phone_SSID;			// Set SSID For Access Point
	String ap_pass = AP_Phone_PASS;
	char AP_SSID[200];
	char AP_PASS[200];
	ap_ssid.toCharArray(AP_SSID, 200);
	ap_pass.toCharArray(AP_PASS, 200);
	WiFi.mode(WIFI_AP);
	WiFi.softAPConfig(IPAddress(192, 168, 4, 1), IPAddress(192, 168, 4, 1), IPAddress(255, 255, 255, 0));
	delay(500);
	WiFi.softAP(AP_SSID, AP_PASS);					// Start Access Point
	IPAddress myIP = WiFi.softAPIP();
	Serial.println(myIP);
	delay(500);
	wifiServer_Phone = WiFiServer(IPAddress(192, 168, 4, 1), 5665);
	wifiServer_Phone.begin();									// Start Server
	delay(500);
	Config_Mode = 1;
	Server_Connected = 0;
	Wifi_Connected = 0;
	Connection_Type = 0;
	Active_Mode = 2;									// Active In Access Point Mode
}

void Function::Command_Phone_Switch()
{
	Main_SSID = Given_ssid;
	Main_PASS = Given_pass;
	User = Given_User;
	Rule = Given_Rule;
	GateWay_Token = Given_Token;
	String Res = Send_Jason(GateWay_Type, GateWay_Token, 1, Type_From, Token_From, Rule_From, Time, "Response", Action, Command, "");
	Serial.println(Res);
	char res[1000];
	Res.toCharArray(res, 1000);
	client_Phone.print(res);
	delay(100);
	Registerd = 65535;
	EEPROM_writeString(0, String(Registerd));
	delay(10);
	EEPROM_writeString(10, GateWay_Token);
	Connection_Type = 2;						// Connect To Wifi 
	Active_Mode = 0;
	Config_Mode = 0;
}

//void Function::Command_Server_Switch()
//{
//	if (Type_To == Switch_Type && Token_To == Switch_Token)
//	{
//		switch (Command)
//		{
//		case 1:
//		{
//			if (Type_Info == "Request")
//			{
//				if (Action == "Get")
//				{
//					Serial.println("1-Request-Get");
//					String Res = Send_Jason(Switch_Type, Switch_Token, 1, Type_From, Token_From, Rule_From, Time, "Response", Action, Command, String(""));
//					Send_To_Server(Res);
//				}
//			}
//			break;
//		}
//		case 2:
//		{
//			if (Type_Info == "Request")
//			{
//				if (Action == "Set")
//				{
//					Serial.println("2-Request-Set");
//					Switch_Status = Given_Status;
//					Switch_Status_Change = 1;
//					String Res = Send_Jason(Switch_Type, Switch_Token, 1, Type_From, Token_From, Rule_From, Time, "Response", Action, Command, Switch_Status);
//					Send_To_Server(Res);
//				}
//				else if (Action == "Get")
//				{
//					Switch_Status = Switch_Status_Array[0];
//					Serial.println("2-Request-Get");
//					String Res = Send_Jason(Switch_Type, Switch_Token, 1, Type_From, Token_From, Rule_From, Time, "Response", Action, Command, Switch_Status);
//					Send_To_Server(Res);
//				}
//			}
//			else if (Type_Info == "Response")
//			{
//				if (Action == "Get")
//				{
//					Serial.println("2-Response-Get");
//					//Switch_Status = Given_Status;
//					Switch_Status_Change = 1;
//				}
//				else if (Action == "Get-N")
//				{
//					Serial.println("2-Response-Get-N");
//					//Switch_Status = "0-0";
//					Switch_Status_Array[0] = "0";
//					Switch_Status_Change = 1;
//				}
//			}
//			break;
//		}
//		case 7:
//		{
//			if (Type_Info == "Request")
//			{
//				if (Action == "Set")
//				{
//					Serial.println("7-Request-Get");
//					String Res = Send_Jason(Switch_Type, Switch_Token, 1, Type_From, Token_From, Rule_From, Time, "Response", Action, Command, "");
//					Send_To_Server(Res);
//					Start_OTA(Link_OTA);
//				}
//			}
//			break;
//		}
//		case 15:
//		{
//			switch (Given_Active_Mode)
//			{
//			case 1:
//			{
//				AP_SSID = Given_ssid;
//				AP_PASS = Given_pass;
//				String Res = Send_Jason(Switch_Type, Switch_Token, 1, Type_From, Token_From, Rule_From, Time, "Response", Action, Command, "");
//				Send_To_Server(Res);
//				Connection_Type = 5;						// Start Access Point Mode 
//				Active_Mode = 0;
//				break;
//			}
//			case 2:
//			{
//				Main_SSID = Given_ssid;
//				Main_PASS = Given_pass;
//				EEPROM_writeString(100, Main_SSID);
//				EEPROM_writeString(150, Main_PASS);
//				Serial.print("SSID : ");
//				Serial.println(Main_SSID);
//				Serial.print("PASS : ");
//				Serial.println(Main_PASS);
//				String Res = Send_Jason(Switch_Type, Switch_Token, 1, Type_From, Token_From, Rule_From, Time, "Response", Action, Command, "");
//				Send_To_Server(Res);
//				WiFi.disconnect(true);
//				delay(100);
//				Connection_Type = 2;						// Start Station Mode
//				Active_Mode = 0;
//				break;
//			}
//			}
//			break;
//		}
//		case 4:
//		{
//			Registerd = 0;
//			EEPROM_writeString(0, String(Registerd));
//			ESP.restart();
//			break;
//		}
//		case 8:
//		{
//			ESP.restart();
//			break;
//		}
//		}
//	}
//	else
//	{
//		Serial.println("Bridge Bridge");
//		Server_Bridge = 1;
//	}
//}

//void Function::Start_OTA(String Link)
//{
//
//}

void Function::Allowed_AccessPoint_Manager()
{
	int Start = 1;
	for (int i = 1; i <= AA.length(); i++)
	{
		if (AA[i] == '/')
		{
			Allowed_SSIDs[Num_Allowed_SSIDs] = AA.substring(Start, i);
			Serial.println(Allowed_SSIDs[Num_Allowed_SSIDs]);
			Num_Allowed_SSIDs++;
			Start = i + 1;
		}
	}
	//Allowed_SSIDs[Num_Allowed_SSIDs] = Main_SSID;
	Serial.println(Allowed_SSIDs[Num_Allowed_SSIDs]);
	//Num_Allowed_SSIDs++;
}

void Function::Manage_Clients(String Data)
{
	DynamicJsonDocument Massage(2048);
	deserializeJson(Massage, Data);
	String From = Massage["From"];
	char from[100];
	From.toCharArray(from, 100);
	DynamicJsonDocument jason_From(100);
	deserializeJson(jason_From, from);
	String Token_from = jason_From["Token"];
	Serial.println(Token_from);
	if (Token_from != "null")
	{
		if (Token_Exist_Check(Token_from))
		{
			char data[1000];
			Data.toCharArray(data, 1000);
			Client[Client_Pointer].print(data);
			Serial.print(Token_from);
			Serial.println(" To Server");
		}
		else
		{
			Data_Client_Bridge = Data;
			Serial.println("New Client");
			Serial.println("Adding...");
			Add_New_Client(Token_from);
			New_Connection = 1;
			//char data[1000];
			//Data.toCharArray(data, 1000);
			//Client[Client_Pointer].print(data);
			//Serial.print(Token_from);
			//Serial.println(" To Server");
		}
	}
}

bool Function::Token_Exist_Check(String Token)
{
	for (int i_Search_Token = 0 ; i_Search_Token <= Num_Clients - 1 ; i_Search_Token++)
	{
		if (Client_Valid[i_Search_Token] && Client_Token[i_Search_Token] == Token)
		{
			Client_Pointer = i_Search_Token;
			return 1;
		}
	}
	return 0;
}

void Function::Add_New_Client(String Token)
{
	for (int i_Search_blank = 0; i_Search_blank < Max_Clients; i_Search_blank++)
	{
		if (!Client_Valid[i_Search_blank])
		{
			Client_Token[i_Search_blank] = Token;
			Client_Valid[i_Search_blank] = 1;
			Client_Pointer = i_Search_blank;
			Serial.println(i_Search_blank);
			break;
		}
	}
	Num_Clients++;
	Serial.println("Added.");
}

void Function::StayToResponse()
{
	if (client_GateWay.available())                                     // If There Is Any Message From Server
	{
		String Received_Data = client_GateWay.readStringUntil('\n');    // Read Message
		Serial.print("Receive Server : ");
		Serial.println(Received_Data);
		Connection_Type = 4;
	}
}