#pragma once
class Function
{
public:
	void Connect_To_WiFi();
	bool Manage_WiFi();
	void Connect_To_Server();
	void Start_AP();
	void Start_WiFiServer();
	void Switch_Manager();
	void Wifi_LED();
	void EEPROM_writeString(char add, String data);
	String EEPROM_readString(char add);
	bool Recive_Jason(String message);
	String Send_Jason(String Type_From, String Token_From, int Rule_From, String Type_To, String Token_To, int Rule_To, String Time, String Type_Info, String Action, int Command, String Status);
	void Send_To_Server(String Massege);
	String getMacAddress();
	void Start_AP_Phone();
	void Command_Phone_Switch();
	void Command_Server_Switch();
	//void Start_OTA(String Link);
	void Allowed_AccessPoint_Manager();
	void Manage_Clients(String Data);
	bool Token_Exist_Check(String Token);
	void Add_New_Client(String Token);
	void StayToResponse();
};

