#pragma once
#include <ArduinoJson.hpp>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiAP.h>
#include <WiFiClient.h>
#include <WiFiServer.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>
#include "EEPROM.h"
#include "Function.h"

extern int Registerd;
extern String Token_From;
extern String Type_From;
extern int Rule_From;
extern String Token_To;
extern String Type_To;
extern int Rule_To;
extern String Time;
extern String Type_Info;
extern String Action;
extern int Command;
extern String Given_ssid;
extern String Given_pass;
extern String Given_User;
extern int Given_Rule;
extern String Given_Token;
extern String Given_Status;
extern String Switch_Status_Array[1];
extern bool Given_Factory_Reset;
extern String Link_OTA;
extern int Given_Active_Mode;



extern String User;
extern int Rule;
extern String GateWay_Token;
extern bool Factory_Reset;
extern String GateWay_Type;
extern bool Touch1;
extern int Touch1_State;
extern unsigned long Touch1_Timer;
extern unsigned long Timer_LED;
extern String Switch_Status;

extern String Server_ID;

extern String ST_SSID;
extern String ST_PASS;
extern String AP_SSID;
extern String AP_PASS;
extern String AP_Phone_SSID;
extern String AP_Phone_PASS;
extern String Main_SSID;
extern String Main_PASS;
extern String AA;
extern String IP;
extern String PORT;
extern String Server_IP;
extern String Server_PORT;
extern String Node_IP;
extern String Node_PORT;

//extern bool Switch_Status_Change;
extern bool Wifi_Connected;
extern int chipid;
//extern int LED1_Pin;
//extern int Relay1_Pin;
extern int Wifi_LED_Pin;
extern int Touch1_Pin;
extern int Connection_Type;
extern int Active_Mode;
extern unsigned long Last_Scan_Network;
extern int Max_Power;
extern int Num_Allowed_SSIDs;
extern String Allowed_SSIDs[10];
extern int try_wifi;
extern bool Client_Bridge;
extern bool Server_Bridge;
extern String Data_Client_Bridge;
extern String Data_Server_Bridge;
extern unsigned long Last_Check_Connection;
extern bool Config_Mode;
extern bool Check_Client;
extern bool New_Client;
extern int Num_Clients;
extern int Max_Clients;
extern String Client_Token[20];
extern bool Client_Valid[20];
extern int Client_Pointer;
extern bool New_Connection;
extern Function F;
extern WiFiClient client_DOWN;
extern WiFiClient client_Phone;
extern WiFiClient client_GateWay;
extern WiFiServer wifiServer;
extern WiFiServer wifiServer_Phone;
extern WiFiClient Client[20];
extern ESP8266WebServer server;
extern char MAIN_page[] PROGMEM;
extern bool Server_Connected;
class Global
{
};

